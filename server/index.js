var app = require('express')()
var cors = require('cors')

var openTank = false
var sensorValue = 0

app.use(cors())

app.get('/s/:value', function(req, res) {
  console.log('GET /s/' + req.params.value)
  sensorValue = req.params.value
  res.send('y\r')
})

app.get('/gs', function(req, res) {
  res.send(sensorValue.toString())
})

app.get('/o', function(req, res) {
  if(openTank) {
    console.log("GET /o t")
    res.send('t\r')
  } else {
    console.log("GET /o f")
    res.send('f\r')
  }
  openTank = false
})

app.get('/so', function(req, res) {
  console.log("GET /so")
  if(openTank === true) {
    res.status(400)
  } else {
    openTank = true
    res.send('s')
  }
})

app.listen(5000, function() {
  console.log('listening at port 5000')
})
