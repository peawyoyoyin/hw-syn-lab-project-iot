/*
 * errno.h
 *
 *  Created on: Nov 21, 2017
 *      Author: Kris
 */

#ifndef CAT_ERRNO_H_
#define CAT_ERRNO_H_


#include "stm32f4xx_hal.h"

#define AGN_ERRNO_UNKNOWN 			1
#define AGN_ERRNO_RESET_ERRNO 		2
#define AGN_ERRNO_LOGGING_FAILED 	3

void resetErrno();
void setErrno(int errCode);

int getErrno();


#endif /* CAT_ERRNO_H_ */
