#include <SoftwareSerial.h>

#include <ESP8266WiFi.h>

#define USE_SERIAL Serial

#define rxPin 4
#define txPin 5


const char* ssid     = "AndroidAPw";
const char* password = "msqa6833";

const char* host = "192.168.43.35";
const char* streamId   = "....................";
const char* privateKey = "....................";


SoftwareSerial sSerial(rxPin, txPin);
uint8_t lastByte;
char serialBuffer[64];
int count = 0;
int onboardLed = LED_BUILTIN;

void setup() {
  
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  Serial.begin(115200);
  sSerial.begin(115200);
  pinMode(onboardLed, OUTPUT);
  digitalWrite(onboardLed, HIGH);
  Serial.println("Initialized Serial Communication"); 

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}

void httpRequest(String url) {
//  Serial.print("connecting to ");
//  Serial.println(host);
  
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 5000;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }
  
  Serial.print("Requesting URL: ");
  Serial.println(url);
  
  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
  
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
//    Serial.print(line);
  }
  
  Serial.println();
//  Serial.println("closing connection");
}

int checkOpenValve() {
//  Serial.print("connecting to ");
//  Serial.println(host);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 5000;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return 0;
  }

  String url = "/o";
  
//  Serial.print("Requesting URL: ");
//  Serial.println(url);
  
  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return 0;
    }
  }
  
  // Read all the lines of the reply from server and print them to Serial
  String line;
  while(client.available()){
    line = client.readStringUntil('\r');
//    Serial.print(line);
  }
//  Serial.println("line : " + String(line[1]));

  if(line[1] == 't') {
    Serial.println("Open Valve");
    return 1;
  } else if(line[1] == 'f') {
//    Serial.println("Doing nothing");
  } else {
//    Serial.println("Error?");
  }

  return 0;
//  Serial.println();
//  Serial.println("closing connection");  
}

void loop() {
  char c = 's';
  while (sSerial.available() > 0) {
    c = sSerial.read();
    if(c == '\n') {break;}
    serialBuffer[count] = c;
    count++;
  }
  if(c == '\n') {
    if(count > 0) {
      char completeURL[200] = "/s/";
      for(int i=0; i<count; i++) {
        completeURL[i+3] = serialBuffer[i];
      }
      httpRequest(String(completeURL));      
    }
    count=0;
  }
  if(checkOpenValve() == 1) {
    sSerial.write('o');
  }
  delay(100);
}
