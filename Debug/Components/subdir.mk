################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Components/cat_gateway.c \
../Components/cat_logger.c \
../Components/cat_range_finder.c 

OBJS += \
./Components/cat_gateway.o \
./Components/cat_logger.o \
./Components/cat_range_finder.o 

C_DEPS += \
./Components/cat_gateway.d \
./Components/cat_logger.d \
./Components/cat_range_finder.d 


# Each subdirectory must supply rules for building sources it contributes
Components/%.o: ../Components/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F407xx -I"F:/ProjectLab/Components" -I"F:/ProjectLab/Drivers/STM32F4xx_HAL_Driver/Inc" -I"F:/ProjectLab/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"F:/ProjectLab/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"F:/ProjectLab/Drivers/CMSIS/Include" -I"F:/ProjectLab/Inc"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


