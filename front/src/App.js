import React, { Component } from 'react';
import { Circle } from 'rc-progress';
import Cat from './Water Cat.png';
import './App.css';

var MAX_SENSOR_VALUE = 12000

class App extends Component {
  constructor() {
    super()
    this.state = {
      sensorValue: -1,
      percentage: 10
    }
    this.getSensorData = this.getSensorData.bind(this)
    setInterval(() => {
      this.getSensorData()
    }, 100)
  }

  getSensorData() {
    fetch('http://localhost:5000/gs')
    .then(res => res.text())
    .then(text => {
      const sensorValue = parseInt(text)
      const percentage = (1 - (sensorValue / MAX_SENSOR_VALUE)) * 100
      this.setState({
        sensorValue,
        percentage
      })
    })
  }

  openValve() {
    fetch('http://localhost:5000/so')
  }

  render() {
    return (
      <div className="app">
      <div className="div-bg1"></div>
      <div className="div-bg2"></div>
      <div><img src={ Cat } className="cat"/></div>
      <span className="water-cat">Water Cat</span>
        <Circle className="pc cat-bowl" percent={this.state.percentage} strokeWidth="6" strokeColor="#772fed" />
        {/* <span className="percentage">{this.state.percentage}</span> */}
        <span className="percentage">{this.state.sensorValue}</span>
      <button className="button" onClick={this.openValve}><span className="fill-your-cat">Fill Your Cat</span></button>
      </div>
    );
  }
  
}

export default App;
